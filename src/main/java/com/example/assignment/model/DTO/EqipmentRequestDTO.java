package com.example.assignment.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EqipmentRequestDTO {

    private Long id;
    private String employeeName;
    private String equipmentName;
    private String mentorName;
    private boolean approved;
    private String type;

}
