package com.example.assignment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "equipment")
    @JsonIgnore
    private List<Employee> employees;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipmentR")
    @JsonIgnore
    private List<Equipment_request> equipment_requests = new ArrayList<>();
}
