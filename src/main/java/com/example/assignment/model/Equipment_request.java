package com.example.assignment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Equipment_request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private Mentor mentorR;

    @ManyToOne
    @JsonIgnore
    private Employee employeeR;

    @ManyToOne
    @JsonIgnore
    private Equipment equipmentR;

    private boolean approved = false;
    private String type;
}
