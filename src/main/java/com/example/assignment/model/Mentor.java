package com.example.assignment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.jfr.Enabled;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Mentor extends User {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mentor")
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mentorR")
    @JsonIgnore
    private List<Equipment_request> equipment_requests = new ArrayList<>();
}
