package com.example.assignment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Employee extends User {

    @ManyToOne
    @JsonIgnore
    private Mentor mentor;

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "employee_equipment",
            joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "equipment_id"))
    private List<Equipment> equipment = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeR")
    @JsonIgnore
    private List<Equipment_request> equipment_requests = new ArrayList<>();
}
