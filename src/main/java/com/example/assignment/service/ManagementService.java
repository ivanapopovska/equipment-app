package com.example.assignment.service;

import com.example.assignment.model.DTO.EqipmentRequestDTO;
import com.example.assignment.model.Equipment_request;
import com.example.assignment.model.Management;
import com.example.assignment.repository.ManagementRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManagementService {


    private final ManagementRepo managementRepo;
    private final RequestService requestService;

    public ManagementService(ManagementRepo managementRepo, RequestService requestService) {
        this.managementRepo = managementRepo;
        this.requestService = requestService;
    }

    public Management save(Management management){
        return managementRepo.save(management);
    }

    public Iterable<EqipmentRequestDTO> getAllApproved() {
        List<Equipment_request> equipment_requests = (List<Equipment_request>) requestService.getAll();
        List<EqipmentRequestDTO> requestDTOS = new ArrayList<>();
        for(Equipment_request er : equipment_requests){
            if(er.isApproved()){
                EqipmentRequestDTO eqipmentRequestDTO = new EqipmentRequestDTO();
                eqipmentRequestDTO.setId(er.getId());
                eqipmentRequestDTO.setEmployeeName(er.getEmployeeR().getFirstName());
                eqipmentRequestDTO.setEquipmentName(er.getEquipmentR().getName());
                eqipmentRequestDTO.setApproved(er.isApproved());
                eqipmentRequestDTO.setMentorName(er.getMentorR().getFirstName());
                eqipmentRequestDTO.setType(er.getType());
                requestDTOS.add(eqipmentRequestDTO);
            }
        }
        return requestDTOS;
    }
}
