package com.example.assignment.service;

import com.example.assignment.model.Employee;
import com.example.assignment.model.Equipment;
import com.example.assignment.model.User;
import com.example.assignment.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private final EmployeeRepo employeeRepo;

    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public Employee save(Employee employee){
        return employeeRepo.save(employee);
    }
    public Employee getEmployeeById(Long id){
        return employeeRepo.findById(id).orElse(null);
    }

    public Iterable<Employee> getAll() {
        return employeeRepo.findAll();
    }

    public Iterable<Equipment> getAllByEmployee(Long id){
        Employee employee = employeeRepo.findById(id).orElse(null);
        List<Equipment> equipment = employee.getEquipment();
        return equipment;
    }
}
