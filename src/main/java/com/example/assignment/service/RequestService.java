package com.example.assignment.service;

import com.example.assignment.model.DTO.EqipmentRequestDTO;
import com.example.assignment.model.Employee;
import com.example.assignment.model.Equipment;
import com.example.assignment.model.Equipment_request;
import com.example.assignment.model.Mentor;
import com.example.assignment.repository.RequestRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestService {
    private final EquipmentService equipmentService;
    private final EmployeeService employeeService;
    private final MentorService mentorService;
    private final RequestRepo requestRepo;

    public RequestService(EquipmentService equipmentService, EmployeeService employeeService, MentorService mentorService, RequestRepo requestRepo) {
        this.equipmentService = equipmentService;
        this.employeeService = employeeService;
        this.mentorService = mentorService;
        this.requestRepo = requestRepo;
    }

    public Equipment_request sendRequest(Long equipmentId, Long employeeId) {
        Equipment equipment = equipmentService.getEquipmentById(equipmentId);
        Employee employee = employeeService.getEmployeeById(employeeId);
        Equipment_request equipment_request = new Equipment_request();
        equipment_request.setEmployeeR(employee);
        equipment_request.setEquipmentR(equipment);
        equipment_request.setMentorR(employee.getMentor());
        List<Equipment> employeeEquipment = employee.getEquipment();
        if(employeeEquipment.contains(equipment)){
            equipment_request.setType("replacement");
        }else{
            equipment_request.setType("additional");
        }
        return requestRepo.save(equipment_request);

    }

    public Iterable<EqipmentRequestDTO> getAllRequestByMentor(Long mentorId) {
        Mentor mentor = mentorService.getMentorById(mentorId);
        List<Equipment_request> equipment_requests = mentor.getEquipment_requests();
        List<EqipmentRequestDTO> requestDTOS = new ArrayList<>();
        for(Equipment_request er : equipment_requests){
            EqipmentRequestDTO eqipmentRequestDTO = new EqipmentRequestDTO();
            eqipmentRequestDTO.setId(er.getId());
            eqipmentRequestDTO.setEmployeeName(er.getEmployeeR().getFirstName());
            eqipmentRequestDTO.setEquipmentName(er.getEquipmentR().getName());
            eqipmentRequestDTO.setApproved(er.isApproved());
            eqipmentRequestDTO.setMentorName(er.getMentorR().getFirstName());
            eqipmentRequestDTO.setType(er.getType());
            requestDTOS.add(eqipmentRequestDTO);
        }
        return requestDTOS;
    }

    public Equipment_request approveRequest(Long requestId) {
        Equipment_request equipment_request = requestRepo.findById(requestId).orElse(null);
        equipment_request.setApproved(!equipment_request.isApproved());
        return requestRepo.save(equipment_request);
    }

    public Iterable<Equipment_request> getAll(){
        return requestRepo.findAll();
    }

    public void deleteRequest(Long requestId) {
        requestRepo.deleteById(requestId);
    }
}
