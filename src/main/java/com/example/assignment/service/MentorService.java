package com.example.assignment.service;

import com.example.assignment.model.Mentor;
import com.example.assignment.repository.MentorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MentorService {

    @Autowired
    private final MentorRepo mentorRepo;

    public MentorService(MentorRepo mentorRepo) {
        this.mentorRepo = mentorRepo;
    }

    public Mentor save (Mentor mentor){
        return mentorRepo.save(mentor);
    }
    public Mentor getMentorById(Long id){
        return  mentorRepo.findById(id).orElse(null);
    }

    public Iterable<Mentor> getAll() {
        return mentorRepo.findAll();
    }
}
