package com.example.assignment.service;

import com.example.assignment.model.Equipment;
import com.example.assignment.repository.EquipmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipmentService {

    @Autowired
    private final EquipmentRepo equipmentRepo;

    public EquipmentService(EquipmentRepo equipmentRepo) {
        this.equipmentRepo = equipmentRepo;
    }

    public Equipment save(Equipment equipment){
        return equipmentRepo.save(equipment);
    }

    public Iterable<Equipment> getAll() {
        return equipmentRepo.findAll();
    }

    public Equipment getEquipmentById(Long id){
        return equipmentRepo.findById(id).orElse(null);
    }
}
