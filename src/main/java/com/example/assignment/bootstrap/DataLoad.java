package com.example.assignment.bootstrap;

import com.example.assignment.model.Employee;
import com.example.assignment.model.Equipment;
import com.example.assignment.model.Management;
import com.example.assignment.model.Mentor;
import com.example.assignment.service.EmployeeService;
import com.example.assignment.service.EquipmentService;
import com.example.assignment.service.ManagementService;
import com.example.assignment.service.MentorService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoad implements CommandLineRunner {

    private final EquipmentService equipmentService;
    private final EmployeeService employeeService;
    private final ManagementService managementService;
    private final MentorService mentorService;

    public DataLoad(EquipmentService equipmentService, EmployeeService employeeService, ManagementService managementService, MentorService mentorService) {
        this.equipmentService = equipmentService;
        this.employeeService = employeeService;
        this.managementService = managementService;
        this.mentorService = mentorService;
    }

    @Override
    public void run(String... args) throws Exception {
        loadData();
    }

    private void loadData() {
        Equipment monitor = new Equipment();
        monitor.setName("monitor");
        equipmentService.save(monitor);
        Equipment mouse = new Equipment();
        mouse.setName("mouse");
        equipmentService.save(mouse);
        Equipment headphones = new Equipment();
        headphones.setName("headphones");
        equipmentService.save(headphones);
        Equipment laptop = new Equipment();
        laptop.setName("laptop");
        equipmentService.save(laptop);
        Equipment keyboard = new Equipment();
        keyboard.setName("keyboard");
        equipmentService.save(keyboard);
        Equipment printer = new Equipment();
        printer.setName("printer");
        equipmentService.save(printer);

        Employee martin = new Employee();
        martin.setFirstName("Martin");
        martin.setLastName("Martinovski");
        martin.getEquipment().add(monitor);
        martin.getEquipment().add(mouse);
        martin.getEquipment().add(headphones);
        martin.getEquipment().add(laptop);

        Mentor andrej = new Mentor();
        andrej.setFirstName("Andrej");
        andrej.setLastName("Andreevski");
        martin.setMentor(andrej);
        mentorService.save(andrej);
        employeeService.save(martin);


        Management sara = new Management();
        sara.setFirstName("Sara");
        sara.setLastName("Sarovska");
        managementService.save(sara);

    }
}
