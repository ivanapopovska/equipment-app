package com.example.assignment.web;


import com.example.assignment.model.DTO.EqipmentRequestDTO;
import com.example.assignment.model.Equipment_request;
import com.example.assignment.model.User;
import com.example.assignment.repository.MentorRepo;
import com.example.assignment.repository.UserRepo;
import com.example.assignment.service.RequestService;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/request")
public class RequestController {

    private final RequestService requestService;
    private final UserRepo userRepo;

    public RequestController(RequestService requestService, UserRepo userRepo) {
        this.requestService = requestService;
        this.userRepo = userRepo;
    }

    @PostMapping("/{equipmentId}/{employeeId}")
    public Equipment_request sendRequest(@PathVariable Long equipmentId, @PathVariable Long employeeId){
        return requestService.sendRequest(equipmentId, employeeId);
    }

    @GetMapping("/{mentorId}")
    public Iterable<EqipmentRequestDTO> getAllRequestByMentor(@PathVariable Long mentorId){
        return requestService.getAllRequestByMentor(mentorId);
    }
    @PatchMapping("/{requestId}")
    public Equipment_request approveRequest(@PathVariable Long requestId){
        return requestService.approveRequest(requestId);
    }
    @DeleteMapping("/delete/{requestId}")
    public void deleteRequest(@PathVariable Long requestId){
        requestService.deleteRequest(requestId);
    }

}
