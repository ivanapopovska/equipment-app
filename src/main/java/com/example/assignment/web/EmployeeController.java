package com.example.assignment.web;

import com.example.assignment.model.Employee;
import com.example.assignment.model.Equipment;
import com.example.assignment.model.User;
import com.example.assignment.service.EmployeeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<Employee> getAllEmployees(){
        return employeeService.getAll();
    }

    @GetMapping("/{employeeId}")
    public Iterable<Equipment> getAllByEmployee(@PathVariable Long employeeId){
        return employeeService.getAllByEmployee(employeeId);
    }
}
