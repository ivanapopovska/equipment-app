package com.example.assignment.web;


import com.example.assignment.model.DTO.EqipmentRequestDTO;
import com.example.assignment.service.ManagementService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/management")
public class ManagementController {

    private final ManagementService managementService;

    public ManagementController(ManagementService managementService) {
        this.managementService = managementService;
    }

    @GetMapping
    public Iterable<EqipmentRequestDTO> getAllApprovedRequests(){
        return managementService.getAllApproved();
    }
}
