package com.example.assignment.repository;

import com.example.assignment.model.Mentor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MentorRepo extends JpaRepository<Mentor, Long> {
}
