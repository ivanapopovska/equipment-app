package com.example.assignment.repository;

import com.example.assignment.model.Equipment_request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepo extends JpaRepository<Equipment_request, Long> {
}
